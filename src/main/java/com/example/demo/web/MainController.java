package com.example.demo.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.BoardEntity;
import com.example.demo.service.BoardService;

@RestController
public class MainController {

	@Autowired private BoardService boardService;
	
	@RequestMapping(value = "/index")
	public void index() {
		System.out.println(boardService.findAll());
		
	}
	
	
	@RequestMapping(value = "/insert")
	public void insert(BoardEntity board) {
		board.setRegId("test");
		board.setTitle("�׽�Ʈ11234");
		board.setContents("1");
		boardService.save(board);
		
	}
	
}
