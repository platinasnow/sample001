package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Service;

import com.example.demo.domain.BoardEntity;
import com.example.demo.repository.BoardRepository;

@Service
public class BoardService {

	@Autowired private BoardRepository boardRepository;
	
	public Iterable<BoardEntity> findAll(){
		return boardRepository.findAll(PageRequest.of(0, 10).getSortOr(Sort.by(Order.desc("idx"))));
	}
	
	public void save(BoardEntity board) {
		boardRepository.save(board);
	}
	
	
}
