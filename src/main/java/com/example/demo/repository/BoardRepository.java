package com.example.demo.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.demo.domain.BoardEntity;

public interface BoardRepository extends PagingAndSortingRepository<BoardEntity, Integer>{

	//public Page<BoardEntity> findAllOrderByIdxDesc(Pageable pageable);
	
}
